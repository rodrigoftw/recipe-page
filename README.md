# Recipe Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://vigilant-wilson-9f7339.netlify.app/) | [Solution](https://devchallenges.io/solutions/0GlXWyMzSlmSuzQ2Vvzx) | [Challenge](https://devchallenges.io/challenges/OEKdUZ6xs0h99C38XVht) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FrecipeBlogThumbnail.png%3Falt%3Dmedia%26token%3D2d696d3c-a8cb-4c7c-907b-561ae1144cc9&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/OEKdUZ6xs0h99C38XVht) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)

## Features

This project features a responsive page that goes from a small mobile (320px width) view up to a desktop (4K) view.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
